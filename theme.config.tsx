import React from 'react';
import { DocsThemeConfig } from 'nextra-theme-docs';
import SymmetricLogo from './src/components/symmetric-logo';
import { FaGitlab } from 'react-icons/fa';

const config: DocsThemeConfig = {
  logo: (
    <div style={{ display: 'flex', gap: '1rem', alignItems: 'center' }}>
      <SymmetricLogo size={44} />
      <span style={{ fontSize: '1.5rem' }}>Symmetric</span>
    </div>
  ),
  logoLink: 'https://www.symmetric.run',
  head: (
    <>
      <link
        rel="icon"
        type="image/x-icon"
        href="/favicon-light.ico"
        media="(prefers-color-scheme: light)"
      />
      <link
        rel="icon"
        type="image/x-icon"
        href="/favicon-dark.ico"
        media="(prefers-color-scheme: dark)"
      />
    </>
  ),
  useNextSeoProps() {
    return {
      titleTemplate: '%s - Symmetric',
    };
  },
  project: {
    link: 'https://gitlab.com/Jpac14/symmetric',
    icon: <FaGitlab size={20} />,
  },
  docsRepositoryBase:
    'https://gitlab.com/Jpac14/symmetric-docs/-/tree/master/src/pages',
  banner: {
    key: 'alpha-release',
    text: (
      <a href="https://www.symmetric.run" target="_blank">
        🎉 Symmetric is alpha. Try Now →
      </a>
    ),
  },
  footer: {
    text: 'Symmetric Docs',
  },
};

export default config;
